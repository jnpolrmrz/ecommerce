<?php
require "../templates/template.php";

function getContent(){
}



?>
<h1 class="text-center py-4">CATALOG</h1>

<div class="container">
	<div class="row">
		<?php 

		$products = file_get_contents("../assets/lib/products.json");
		$products_array = json_decode($products, true);
		foreach ($products_array as $indiv_products) {
			?>
			<div class="col-lg-4 py-2">
				<div class="card">
					<img class="card-img-top" height="300px" src="../assets/lib/<?php
					echo $indiv_products['image']; ?>">
					<div class="card-body">
						<h5 class="card-title">
							<?php echo $indiv_products['name']; ?>
						</h5>
						<p class="card-text">
							USD <?php echo $indiv_products['price']; ?>.00
						</p>
						<p class="card-text">
							Descriptions:
							<?php echo $indiv_products['description']; ?>
						</p>
					</div>

					<?php 
					if (isset($_SESSION['email']) && $_SESSION['email'] =="admin@admin.com") {

						?>
						<!-- delete -->
						<div class="card-footer">
							<a class="btn btn-danger w-100"  href="../controllers/process_delete_product.php?name=<?php echo $indiv_products['name']; ?>">Delete

							</a>
						</div>






						<?php
								# code...
					}else if (isset($_SESSION['name'])){
						?>
						<!-- add to cart -->
						<div class="card-footer">
							<form method="POST" action="../controllers/process_addToCart.php">
								<input type="hidden" value="<?php echo $indiv_products['name']; ?>" name="name">
								<input type="number" name="quantity" value="1">

								<button type="submit" class="btn btn-info">
									Add to Cart
								</button>

							</form>
						</div>
						<?php

					}
					?>
					<?php 
					?>



				</div>
			</div>
			<?php
					# code...
		}	



		?>
	</div>
</div>


<?php

?>